<?php

class WhiteRabbit
{
    public function findMedianLetterInFile($filePath)
    {
        return array("letter"=>$this->findMedianLetter($this->parseFile($filePath),$occurrences),"count"=>$occurrences);
    }

    /**
     * Parse the input file for letters.
     * @param $filePath
     */
    private function parseFile ($filePath)
    {
        $file= file_get_contents($filePath);
        return $file;
    }

    /**
     * Return the letter whose occurrences are the median.
     * @param $parsedFile
     * @param $occurrences
     */
    private function findMedianLetter($parsedFile, &$occurrences)
    {
				$arraychar=array();
                for($i=0;$i<strlen($parsedFile);$i++){
                    if (preg_match('/[a-zA-Z]/', $parsedFile[$i])){
                        $arraychar[]=$parsedFile[$i];
                    }              
                }
                $arrayLenght=count($arraychar);
                sort($arraychar);
                $median=null;
                if(count($arraychar)%2==0){
                    $num1=ord($arraychar[$arrayLenght/2]);
                    $num2=ord($arraychar[($arrayLenght-1)/2]);
                    $res=($num1+$num2)/2;
                    $median=chr($res);
                }
                else{
                    $index=($arrayLenght-1)/2;
                    $median=$arraychar[$index];
                }
                
                foreach ($arraychar as $c){
                    if($c==$median){
                        $occurences++;
                    }
                }
                return $median;
    }
}