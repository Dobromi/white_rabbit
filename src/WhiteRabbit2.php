<?php

class WhiteRabbit2
{
    /**
     * return a php array, that contains the amount of each type of coin, required to fulfill the amount.
     * The returned array should use as few coins as possible.
     * The coins available for use is: 1, 2, 5, 10, 20, 50, 100
     * You can assume that $amount will be an int
     */
	 
	 //Dynamic programming algorithm implementation
    public function findCashPayment($amount){
				$coins=array(1,2,5,10,20,50,100);
                $used=array();
                $resultCoins=array();

                $used[0]=0;
                $resultCoins[0]=1;

                for($i=1;$i<=$amount;$i++){
                    $minCoins=$i;
                    $newCoin=1;

                    for($j=0;$j<count($coins);$j++){
                        if($coins[$j]>$i){
                            continue;
                        }
                        if($used[$i-$coins[$j]]+1<$minCoins){
                            $minCoins=$used[$i - $coins[$j]]+1;
                            $newCoin=$coins[$j];
                        }

                    }
                    $used[$i]=$minCoins;
                    $resultCoins[$i]=$newCoin;
                }
                $valueArray=array();
                for($i=$amount;$i>0;){
                    echo $resultCoins[$i].'</br>';
                    $valueArray[]=$resultCoins[$i];
                    $i-=$resultCoins[$i];
                }
                $retArray=array();
                foreach ($coins as $coin) {
                    $retArray[$coin]=0;
                }

                for($i=0;$i<count($valueArray);$i++){
                    $retArray[$valueArray[$i]]++;
                }

                return $retArray;
    }
}